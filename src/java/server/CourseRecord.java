package server;

import java.util.List;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;


public class CourseRecord
{
    public static String start(String path, String xml)
    {
        try
        {
            String coursename_ph = "_NOMECORSO_";
            String codicecorso_ph = "_CODICECORSO_";
            String professor_ph = "_PROFESSOR_";
            String numerolezioni_ph = "_NUMLEZ_";
            String orelezioni_ph = "_ORELEZ_";
            String numeroese_ph = "_NUMESE_";
            String oreese_ph = "_OREESE_";
            String annotazioni_ph = "_ANNOTA_";
            String year_ph = "_YEAR_";

            String argomento_ph = "_ARGOMENTO_";
            String ora_ph = "_ORE_";
            String data_ph = "_DATA_";

            String anno = XMLSupport.getValue(xml, "year");
            String coursename = XMLSupport.getValue(xml, "title");
            String professor = XMLSupport.getValue(xml, "professor");
            String coursecode = XMLSupport.getValue(xml, "code");
            String annotation = XMLSupport.getValue(xml, "annotation");
            List<String> argomenti = XMLSupport.getValues(xml, "lecture");
            List<String> ore = XMLSupport.getAttributes(xml, "lecture", "lenght");
            List<String> tipi = XMLSupport.getAttributes(xml, "lecture", "type");
            List<String> data = XMLSupport.getAttributes(xml, "lecture", "date");

            Integer numerolezioni = 0;
            Integer numeroese = 0;
            Integer orelezioni = 0;
            Integer oreese = 0;

            for(int i=0; i<argomenti.size(); i++)
            {
                Integer ora = Integer.parseInt(ore.get(i));
                String tipo = tipi.get(i);
               
                if(tipo.equals("lesson"))
                {
                    numerolezioni++;
                    orelezioni += ora;

                }
                else
                {
                    numeroese++;
                    oreese += ora;
                }
            }

            WordprocessingMLPackage template = DocSupport.getTemplate(path + "template.docx");

            VariablePrepare.prepare(template);

            DocSupport.replacePlaceholder(template, coursename, coursename_ph);
            DocSupport.replacePlaceholder(template, anno, year_ph);
            DocSupport.replacePlaceholder(template, professor, professor_ph);
            DocSupport.replacePlaceholder(template, coursecode, codicecorso_ph);
            DocSupport.replacePlaceholder(template, numerolezioni.toString(), numerolezioni_ph);
            DocSupport.replacePlaceholder(template, orelezioni.toString(), orelezioni_ph);
            DocSupport.replacePlaceholder(template, numeroese.toString(), numeroese_ph);
            DocSupport.replacePlaceholder(template, oreese.toString(), oreese_ph);
            DocSupport.replacePlaceholder(template, annotation, annotazioni_ph);

            for(int i=0; i<argomenti.size(); i++)
            {
                String type;
                if(tipi.get(i).equals("lesson"))
                    type = "Lezione frontale";
                else
                    type = "Esercitazione";

                DocSupport.replacePlaceholder(template, argomenti.get(i) + " (" + type + ")", argomento_ph);
                DocSupport.replacePlaceholder(template, ore.get(i), ora_ph);
                DocSupport.replacePlaceholder(template, data.get(i), data_ph);
            }

            
                  
            String filename = coursename + "_" + professor + ".docx";
            filename = filename.replace(" ", "");
            
            DocSupport.writeDocxToStream(template, path + filename);

            String link = "<a href=" + filename + ">" + filename +"</a>";

            return link;

        }
        catch(Exception e)
        {
            return "Error = " + e.getMessage();
        }
    }
}
