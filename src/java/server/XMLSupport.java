package server;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLSupport
{
    public static String getValue(String xml, String nodeName) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        NodeList nodes = doc.getElementsByTagName(nodeName);

        if(nodes.getLength() != 0)
        {
            Node node = doc.getElementsByTagName(nodeName).item(0);
            String value = node.getTextContent();
            return value;
        }
        else
            return null;
    }

    public static String getValue(String xml, String nodeName, String subNodeName) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        String value = null;

        NodeList nodes = doc.getElementsByTagName(nodeName);

        if(nodes.getLength() != 0)
        {
            NodeList subs = nodes.item(0).getChildNodes();
            for(int j=0; j<subs.getLength(); j++)
            {
                if(subs.item(j).getNodeName().equals(subNodeName))
                    value = subs.item(j).getTextContent();
            }
            return value;
        }
        else
            return null;
    }

    public static String getAttribute(String xml, String nodeName, String attributeName) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        Node node = doc.getElementsByTagName(nodeName).item(0);

        NamedNodeMap itemAtributes = node.getAttributes();

        String attribute = itemAtributes.getNamedItem(attributeName).getNodeValue();

        return attribute;
    }

    public static List<String> getAttributes(String xml, String nodeName, String attributeName)  throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        NodeList nodes = doc.getElementsByTagName(nodeName);

        List<String> values = new ArrayList<String>();
        for(int i=0; i<nodes.getLength(); i++)
        {
            Node node = nodes.item(i);
            NamedNodeMap itemAtributes = node.getAttributes();
            String attribute = itemAtributes.getNamedItem(attributeName).getNodeValue();
            values.add(attribute);
        }

        return values;
    }
    
    public static List<String> getValues(String xml, String nodeName)  throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        NodeList nodes = doc.getElementsByTagName(nodeName);

        List<String> values = new ArrayList<String>();
        for(int i=0; i<nodes.getLength(); i++)
        {
            values.add(nodes.item(i).getTextContent());
        }

        return values;
    }

    public static List<String> getValues(String xml, String nodeName, String subNodeName) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(xml)));
        doc.getDocumentElement().normalize();

        NodeList nodes = doc.getElementsByTagName(nodeName);

        List<String> values = new ArrayList<String>();
        for(int i=0; i<nodes.getLength(); i++)
        {
            NodeList subs = nodes.item(i).getChildNodes();

            for(int j=0; j<subs.getLength(); j++)
            {
                if(subs.item(j).getNodeName().equals(subNodeName))
                    values.add(subs.item(j).getTextContent());
            }
        }

        return values;
    }
}