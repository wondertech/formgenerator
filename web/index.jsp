<%-- 
    Document   : index
    Created on : 27-May-2014, 22:46:21
    Author     : berta
--%>

<%@page import="server.CourseRecord"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form Generator</title>
    </head>
    <body>
        <% 
        if(request.getParameter("buttonName") != null) 
        {
        %>
            Your course record: <%= CourseRecord.start(getServletContext().getRealPath("/"), request.getParameter("textarea1")) %>
        <%
        }
        %>
        
        <FORM NAME="form1" METHOD="POST">
            <INPUT TYPE="HIDDEN" NAME="buttonName"/>
            Fill information in the XML below and press the button to get your records!
            <BR>
            <TEXTAREA NAME="textarea1" ROWS="30" COLS="80"><record>
<title>Nome Corso</title>
<code>Codice Corso</code>
<year>2013/2014</year>
<professor>Nome Professore</professor>
<annotation>Eventuali annotazioni</annotation>
<lecture date="01.05.2014" type="lesson" lenght="2">Argomento 1</lecture>
<lecture date="02.05.2014" type="lesson" lenght="2">Argomento 2</lecture>
<lecture date="03.05.2014" type="exercise" lenght="4">Argomento 3</lecture>
<lecture date="04.06.2014" type="exercise" lenght="4">Argomento 4</lecture>
<lecture date="05.06.2014" type="lesson" lenght="2">Argomento 5</lecture>
<lecture date="06.06.2014" type="lesson" lenght="2">Argomento 6</lecture>
<lecture date="07.07.2014" type="exercise" lenght="4">Argomento 7</lecture>
<lecture date="08.07.2014" type="lesson" lenght="2">Argomento 8</lecture>
<lecture date="09.07.2014" type="lesson" lenght="2">Argomento 9</lecture>
<lecture date="10.07.2014" type="exercise" lenght="4">Argomento 10</lecture>
<lecture date="11.08.2014" type="lesson" lenght="2">Argomento 11</lecture>
<lecture date="12.08.2014" type="lesson" lenght="2">Argomento 12</lecture>
</record></TEXTAREA>
            <BR>
            <INPUT TYPE="BUTTON" VALUE="Get your course record!" ONCLICK="button1()"/>
        </FORM>
        
        <SCRIPT LANGUAGE="JavaScript">
            function button1()
            {
                document.form1.buttonName.value = "button 1"
                form1.submit()
            }    
        </SCRIPT>
    
    </body>
</html>







